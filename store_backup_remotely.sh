#!/bin/bash

#GOAL: send today local YNH backup to remote server
#REQUIREMENTS: have created ext-backup user and configured ssh access on remote server
#HOWTO: call it via cron every day after the backup script

PREFIX="auto-"
TODAY=`date '+%Y-%m-%d'`

THIS_SERVER="lalter.net"
#wiphi.fr VPS
REMOTE_SERVER=185.163.125.40

rsync -avzP /home/yunohost.backup/archives/$PREFIX$TODAY* ext-backup@$REMOTE_SERVER:/home/ext-backup/$THIS_SERVER/
