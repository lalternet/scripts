#!/bin/bash

## Envoi un résumé des logs postfix
## à rajouter dans la crontab, éxécution tous les jours

/usr/sbin/pflogsumm -u 5 -h 5 -d today /var/log/mail.log | mail -s "Postfix Report of `date`" root@lalter.net
