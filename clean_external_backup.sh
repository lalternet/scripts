#!/bin/bash

#GOAL: Remove external YNH backup older than 7 days
#HOWTO: Call it via cron

cd /home/ext-backup/
find . -maxdepth 2 -mindepth 2 -mtime +6 -type f -name "auto*" -exec rm -rf {} \;
