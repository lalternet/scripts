#!/bin/bash

#From  @djib https://forum.yunohost.org/t/sauvegardes-automatiques-a-j-1-j-7-et-j-30/6495
#Modified by @jaxom on 2018-12-19
#Modified by @SiM on 2019-12-09
#Licenced under GPL. https://www.gnu.org/licenses/gpl.html

#GOAL: Creates a backup and removes backups older than 10 days
#HOWTO: call it via cron as frequently as you want


# Backups are created with the following prefix
PREFIX="auto-"
TODAY=`date '+%Y-%m-%d'`

# directory where backups are stored
cd /home/yunohost.backup/archives

#separation between apps and config
yunohost backup create -n $PREFIX$TODAY-core --system
gzip $PREFIX$TODAY-core.tar
yunohost backup create -n $PREFIX$TODAY-apps --apps dokuwiki rainloop wekan element ihatemoney redirect my_webapp my_webapp__2 my_webapp__3
gzip $PREFIX$TODAY-apps.tar
systemctl stop matrix-synapse.service
yunohost backup create -n $PREFIX$TODAY-synapse --apps synapse
systemctl start matrix-synapse.service
gzip $PREFIX$TODAY-synapse.tar
BACKUP_CORE_ONLY=1 yunohost backup create -n $PREFIX$TODAY-nextcloud --apps nextcloud
gzip $PREFIX$TODAY-nextcloud.tar

#Removal of old backups
find . -maxdepth 1 -mindepth 1 -mtime +9 -exec rm -rf {} \;
